# README #

Fontes de um curso básico sobre microsserciços onde será mostrado o desenvolvimento de uma aplicação de vendas de veículos seguindo vários padrões da arquitetura de microsserviços.

Fonte original: https://github.com/rivaildojunior/curso-microservices-kotlin-micronaut

### Criar, inicializar projetos

* ./gradlew run

### Docker

* ver containers rodando
	- sudo docker ps
* ver containers criados
	- sudo docker ps -a
* iniciar docker
	- sudo docker start <container_id>
* parar docker
	- sudo docker stop <container_id>

### Postgres

* docker run --name ms-postgres --network micronaut-net -e "POSTGRES_PASSWORD=postgres" -p 5432:5432 -d postgres

### Redis

* docker run --name ms-redis -p 6379:6379 -d redis

* ver dados redis
	- docker exec -it <container_id> sh
	- redis-cli
	- keys *
	- get <chave>
	
### Kafka

* docker run -d --name zookeeper-server --network micronaut-net -e ALLOW_ANONYMOUS_LOGIN=yes bitnami/zookeeper:latest
* docker run -d --name kafka-server --network micronaut-net -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper-server:2181 bitnami/kafka:latest

* inicializar
	- ~kafka/bin/zookeeper-server-start.sh ../config/zookeeper.properties
	- ~kafka/bin/kafka-server-start.sh ../config/server.properties

* ver topicos
	- ~kafka/bin/kafka-topics.sh --bootstrap-server localhost:9092 --list
	- ~kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic ms-vendas --from-beginning
	
### Consul

* docker run -p 8500:8500 --name ms-consul consul
